from datetime import datetime, timezone

from zipline.api import record, symbol, order
from zipline import run_algorithm

import pandas as pd

def initialize(context):
    pass

def handle_data(context, data):
    order(symbol('AAPL'), 10)
    record (AAPL=data.current(symbol('AAPL'), 'price'))

start = datetime(2016, 3, 1, tzinfo=timezone.utc)
end = datetime(2016, 4, 1, tzinfo=timezone.utc)
start = pd.Timestamp(start, tz='UTC', offset='C')
end = pd.Timestamp(end, tz='UTC', offset='C')

results = run_algorithm(
        start=start,
        end=end,
        initialize=initialize,
        handle_data=handle_data,
        capital_base=10000,
        data_frequency='daily')

print('Catarina')

#Quantopian

def initialize(context):
    #we selected the securities that we want
    context.security = symbol('TSLA')
    
    #Schedule the function that will run every day at market open
    schedule_function(func=action_function, 
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_open(hours=0, minutes=1))

def action_function(context, data):
    price_history=data.history(assets=context.security, fields='price', bar_count=10, frequency='1d')
    
    average_price=price_history.mean()
    
    current_price=data.current(assets=context.security, fields='price')

    if data.can_trade(context.security):
    #This will buy me 1000$ in TSLA shares
        if current_price > average_price*1.03:
            #We allocate our entire portfolio to TSLA
            order_target_percent(context.security, 1)
        else:
            #This will set my positions to zero
            order_target_percent(context.security, 0)